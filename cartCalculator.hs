import Data.Text (replace, pack, unpack)

addToCart :: (String, Integer) -> [(String, Integer)] -> [(String, Integer)]
addToCart item cart = item : cart

calculateSum :: [(String, Integer)] -> Integer
calculateSum itemList = sum (map snd itemList)

extractName :: [(String, Integer)] -> [String]
extractName itemList = map (\item -> unpack (replace (pack "_") (pack " ") (pack item)))
  $ (map fst itemList)

cartList = addToCart ("apples", 6)
  . addToCart ("loaf of bread", 4)
  . addToCart ("loaf_of_bread", 4)
  $ addToCart ("Fish and Chips", 12) []

totalCost = show (calculateSum cartList)
nameOfItems = extractName cartList

main = print ("Item list: ", nameOfItems, "Total cost of items: " ++ totalCost)
